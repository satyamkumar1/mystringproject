function getFullName(data)
{

        if(data==null || data==undefined)
        {
            return ;
        }
        if(typeof data!="object")
        {
            return ;
        }
        if(Array.isArray(data))
        {
            return ;
        }

            
        let fullName="";
        for(let index in data)
        {
                let tempstring=data[index].toLocaleLowerCase();
                fullName+=tempstring[0].toUpperCase()+tempstring.slice(1)+" ";


        }


            return fullName;

    



}
module.exports=getFullName;